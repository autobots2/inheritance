
// InheritanceView.cpp : implementation of the CInheritanceView class
//

#include "stdafx.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "Inheritance.h"
#endif

#include "InheritanceDoc.h"
#include "InheritanceView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CInheritanceView

IMPLEMENT_DYNCREATE(CInheritanceView, CView)

BEGIN_MESSAGE_MAP(CInheritanceView, CView)
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CInheritanceView::OnFilePrintPreview)
	ON_WM_CONTEXTMENU()
	ON_WM_RBUTTONUP()
END_MESSAGE_MAP()

// CInheritanceView construction/destruction

CInheritanceView::CInheritanceView()
{
	// TODO: add construction code here

}

CInheritanceView::~CInheritanceView()
{
}

BOOL CInheritanceView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// CInheritanceView drawing

void CInheritanceView::OnDraw(CDC* /*pDC*/)
{
	CInheritanceDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: add draw code for native data here
}


// CInheritanceView printing


void CInheritanceView::OnFilePrintPreview()
{
#ifndef SHARED_HANDLERS
	AFXPrintPreview(this);
#endif
}

BOOL CInheritanceView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CInheritanceView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CInheritanceView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

void CInheritanceView::OnRButtonUp(UINT /* nFlags */, CPoint point)
{
	ClientToScreen(&point);
	OnContextMenu(this, point);
}

void CInheritanceView::OnContextMenu(CWnd* /* pWnd */, CPoint point)
{
#ifndef SHARED_HANDLERS
	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
#endif
}


// CInheritanceView diagnostics

#ifdef _DEBUG
void CInheritanceView::AssertValid() const
{
	CView::AssertValid();
}

void CInheritanceView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CInheritanceDoc* CInheritanceView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CInheritanceDoc)));
	return (CInheritanceDoc*)m_pDocument;
}
#endif //_DEBUG


// CInheritanceView message handlers
